use std::collections::HashMap;
use std::env;

use std::fs::File;

use std::path::Path;

use regex::Regex;

use dotenv::dotenv;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use serenity::async_trait;
use serenity::model::channel::Message;
use serenity::model::gateway::Ready;
use serenity::prelude::*;

struct Handler {
    wordle_score_file_path: &'static Path,
}
#[derive(Serialize, Deserialize)]
struct UserData {
    user_data: HashMap<String, i64>,
}

#[async_trait]
impl EventHandler for Handler {
    // Set a handler for the `message` event. This is called whenever a new message is received.
    //
    // Event handlers are dispatched through a threadpool, and so multiple events can be
    // dispatched simultaneously.
    async fn message(&self, ctx: Context, msg: Message) {
        if msg.content == "!ping" {
            // Sending a message can fail, due to a network error, an authentication error, or lack
            // of permissions to post in the channel, so log to stdout when some error happens,
            // with a description of it.
            if let Err(why) = msg.channel_id.say(&ctx.http, "Pong!").await {
                println!("Error sending message: {why:?}");
            }
        } else if msg.content.starts_with("Wordle") || msg.content == "!score" {
            let re = Regex::new(r"Wordle \d+,?\d+ (?P<score>[\dX]/\d+)").unwrap();
            let mut data: UserData;
            if let Ok(user_data_file) = File::open(self.wordle_score_file_path) {
                data =
                    serde_json::from_reader(user_data_file)
                        .expect("Unrecognized file type");
            } else {
                data = UserData {
                    user_data: HashMap::new(),
                };
            }
            if msg.content.starts_with("Wordle") {
                let username = msg.author.name;

                let actual_score = re.captures(msg.content.leak()).unwrap().get(1).unwrap();
                let numerator_raw = actual_score
                    .as_str()
                    .chars()
                    .nth(0)
                    .unwrap();
                let numerator : i64;
                if numerator_raw == 'X' {
                    numerator = 7;
                } else {
                    numerator = numerator_raw
                        .to_digit(10)
                        .expect("Unknown value for numerator") as i64;
                }
                let denominator = actual_score
                    .as_str()
                    .chars()
                    .nth_back(0)
                    .unwrap()
                    .to_digit(10)
                    .expect("Unknown value for denominator")
                    as i64;
                let par: i64 = denominator - 2; //default to 4, not sure current state.
                let dist_from_par: i64 = numerator - par;
                println!("Score received: {:?}", dist_from_par);
                let prev_score = data.user_data.get(&username).unwrap_or(&0_i64);
                let new_score = prev_score + dist_from_par;
                data.user_data.insert(username.clone(), new_score);
                msg.channel_id
                    .say(&ctx.http, format!("{}'s score today was {}!", &username, dist_from_par))
                    .await
                    .expect("Did not send message unexpectedly");
                println!(
                    "Previous score: {:?}",
                    data.user_data.get(username.as_str())
                );
            }
            let cur_standings = data
                .user_data
                .iter()
                .sorted_by(|l, r| l.1.cmp(r.1))
                .map(|(k, v)| format!("{}: {}", k, v))
                .collect::<Vec<_>>()
                .join("\n");
            msg.channel_id
                .say(&ctx.http, format!("Current standings: \n{}", cur_standings))
                .await
                .expect("Message received");
            serde_json::to_writer(File::create(self.wordle_score_file_path).unwrap(), &data)
                .expect("Unable to persist file changes");
        }
    }

    // Set a handler to be called on the `ready` event. This is called when a shard is booted, and
    // a READY payload is sent by Discord. This payload contains data like the current user's guild
    // Ids, current user data, private channels, and more.
    //
    // In this case, just print what the current user's username is.
    async fn ready(&self, _: Context, ready: Ready) {
        println!(
            "{} is connected, loading data from {:?}!",
            ready.user.name,
            self.wordle_score_file_path.to_str()
        );
    }
}

#[tokio::main]
async fn main() {
    dotenv().ok();
    // Configure the client with your Discord bot token in the environment.
    let token = env::var("AUTH_TOKEN").expect("Expected a token in the environment");
    // Set gateway intents, which decides what events the bot will be notified about
    let intents = GatewayIntents::GUILD_MESSAGES | GatewayIntents::MESSAGE_CONTENT;
    let configured_path =
        Box::new(env::var("WORDLE_FILE_PATH").expect("No file path to persist Wordle data found."));
    let wordle_score_file_path = Path::new(configured_path.leak());
    let handler: Handler = Handler {
        wordle_score_file_path,
    };
    // Create a new instance of the Client, logging in as a bot. This will automatically prepend
    // your bot token with "Bot ", which is a requirement by Discord for bot users.
    let mut client = Client::builder(&token, intents)
        .event_handler(handler)
        .await
        .expect("Err creating client");

    // Finally, start a single shard, and start listening to events.
    //
    // Shards will automatically attempt to reconnect, and will perform exponential backoff until
    // it reconnects.
    if let Err(why) = client.start().await {
        println!("Client error: {why:?}");
    }
}
